package com.example.homeworkspring003.model.respone;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;


import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    private T paload;
    private HttpStatus httpStatus;
    private Timestamp timestamp;

}
