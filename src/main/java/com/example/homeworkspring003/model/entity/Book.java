package com.example.homeworkspring003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    private Integer book_id;
    private String title;
    private Timestamp timestamp;
    private Author author;
    private List<Category> categories=new ArrayList<>();
}
