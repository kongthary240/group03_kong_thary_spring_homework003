package com.example.homeworkspring003.controller;

import com.example.homeworkspring003.model.entity.Category;
import com.example.homeworkspring003.model.request.CategoryRequest;
import com.example.homeworkspring003.model.respone.CategoryResponse;
import com.example.homeworkspring003.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all categories")
    public ResponseEntity<CategoryResponse<List<Category>>> getAllCategory(){
        CategoryResponse<List<Category>> response=CategoryResponse.<List<Category>>builder()
                .message("Fetch data success")
                .paload(categoryService.getAllCategory())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get category by id")
    public ResponseEntity< CategoryResponse<Category>> getCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<Category> response=null;
        if(categoryService.getCategoryById(categoryId)!=null){
            response=CategoryResponse.<Category>builder()
                    .message("Fecth data by id is successfully")
                    .paload(categoryService.getCategoryById(categoryId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response=CategoryResponse.<Category>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete category by id")
    public ResponseEntity<CategoryResponse<String >> deleteCategoryById(@PathVariable("id") Integer categoryId){
        CategoryResponse<String> response=null;
        if(categoryService.deleteCategoryById(categoryId) ==true){
                    response=CategoryResponse.<String>builder()
                    .message("Delete data successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }
    @PostMapping
    @Operation(summary = "Add new category")
    public ResponseEntity<CategoryResponse<Category>> addCategory(@RequestBody CategoryRequest categoryRequest){

        Integer newCategory=categoryService.addCategory(categoryRequest);
        if(newCategory!=null){
            CategoryResponse<Category> response=CategoryResponse.<Category>builder()
                    .message("Add sucess")
                    .paload(categoryService.getCategoryById(newCategory))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }return null;
    }
    @PutMapping("{id}")
    @Operation(summary = "Update category by id")
    public ResponseEntity<CategoryResponse<Category>> updateCategoryById(
            @RequestBody CategoryRequest categoryRequest,@PathVariable("id") Integer categoryId
    ){
        CategoryResponse<Category> response=null;
        Integer categoryUpdate=categoryService.updateCategory(categoryRequest,categoryId);
        if(categoryUpdate!=null){
            response=CategoryResponse.<Category>builder()
                    .message("Update  successfully")
                    .paload(categoryService.getCategoryById(categoryUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
