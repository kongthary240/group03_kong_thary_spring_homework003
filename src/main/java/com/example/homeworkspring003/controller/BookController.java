package com.example.homeworkspring003.controller;

import com.example.homeworkspring003.model.entity.Book;
import com.example.homeworkspring003.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get book by id")
    public ResponseEntity<Book> getAllBook(@PathVariable("id") Integer bookId){
        return ResponseEntity.ok(bookService.getBookById(bookId));
    }
    @GetMapping("/all")
    @Operation(summary = "Get all book")
    public ResponseEntity<List<Book>> getAllBook(){
        return ResponseEntity.ok(bookService.getAllBook());
    }

}
