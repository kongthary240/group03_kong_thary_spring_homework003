package com.example.homeworkspring003.controller;

import com.example.homeworkspring003.model.entity.Author;
import com.example.homeworkspring003.model.entity.Category;
import com.example.homeworkspring003.model.request.AuthorRequest;
import com.example.homeworkspring003.model.request.CategoryRequest;
import com.example.homeworkspring003.model.respone.AuthorResponse;
import com.example.homeworkspring003.model.respone.CategoryResponse;
import com.example.homeworkspring003.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {

        this.authorService = authorService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all author")
    public ResponseEntity<AuthorResponse<List<Author>>> getAllAuthor(){
        AuthorResponse<List<Author>> response=AuthorResponse.<List<Author>>builder()
                .message("Fetch data success")
                .paload(authorService.getAllAuthor())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return  ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    @Operation(summary = "Get author by id")
    public ResponseEntity<AuthorResponse<Author>> getAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<Author> response=null;
        if(authorService.getAuthorById(authorId)!=null){
            response=AuthorResponse.<Author>builder()
                    .message("Fecth data by id is successfully")
                    .paload(authorService.getAuthorById(authorId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response=AuthorResponse.<Author>builder()
                    .message("Data not found")
                    .httpStatus(HttpStatus.NOT_FOUND)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }

    }
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete author by id")
    public ResponseEntity<AuthorResponse<String >> deleteAuthorById(@PathVariable("id") Integer authorId){
        AuthorResponse<String> response=null;
        if(authorService.deleteAuthorById(authorId) ==true){
            response=AuthorResponse.<String>builder()
                    .message("Delete data successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }

        return ResponseEntity.ok(response);
    }
    @PostMapping
    @Operation(summary = "Add new author")
    public ResponseEntity<AuthorResponse<Author>> addAuthor(@RequestBody AuthorRequest authorRequest){

        Integer newAuthor=authorService.addAuthor(authorRequest);
        if(newAuthor!=null){
            AuthorResponse<Author> response=AuthorResponse.<Author>builder()
                    .message("Add sucessfully")
                    .paload(authorService.getAuthorById(newAuthor))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }return null;
    }

    @PutMapping("{id}")
    @Operation(summary = "Update author by id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById(
            @RequestBody AuthorRequest authorRequest,@PathVariable("id") Integer authorId
    ){
        AuthorResponse<Author> response=null;
        Integer authorUpdate=authorService.updateAuthor(authorRequest,authorId);
        if(authorUpdate!=null){
            response=AuthorResponse.<Author>builder()
                    .message("Update  successfully")
                    .paload(authorService.getAuthorById(authorUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
