package com.example.homeworkspring003.repository;

import com.example.homeworkspring003.model.entity.Category;
import com.example.homeworkspring003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("SELECT * FROM categories")
    List<Category> findAllCategory();

    @Select("SELECT * FROM categories WHERE category_id = #{categoryId}")
    Category getCategoryById(Integer categoryId);

    @Delete("DELETE FROM categories WHERE category_id = #{categoryId}")
    boolean deleteCategoryById(Integer categoryId);

    @Select("INSERT INTO categories (category_name) VALUES(#{request.category_name})"+
    "RETURNING category_id")
    Integer addCategory(@Param("request")CategoryRequest categoryRequest);

   @Select("""
           UPDATE categories
           SET category_name = #{request.category_name}
           WHERE category_id = #{categoryId}
           RETURNING category_id
           """)
    Integer updateCategory(@Param("request") CategoryRequest categoryRequest,Integer categoryId);

    @Select("SELECT c.category_id ,category_name\n" +
            "FROM book_details b INNER JOIN categories c\n" +
            "    on c.category_id = b.category_id\n" +
            "where b.book_id=#{book_id};")
    List<Category> getCategoryByBookId(Integer book_id);
}
