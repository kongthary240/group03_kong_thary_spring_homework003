package com.example.homeworkspring003.repository;

import com.example.homeworkspring003.model.entity.Book;
import com.example.homeworkspring003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {
    //get all data
    @Result(property = "book_id", column = "book_id")

    @Select("SELECT * FROM books")
    @Result(property = "timestamp",column = "published_date")
    @Result(property = "author",column = "author_id",
            one = @One(select = "com.example.homeworkspring003.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories",column = "book_id",
            many = @Many(select = "com.example.homeworkspring003.repository.CategoryRepository.getCategoryByBookId")
    )
    List<Book> findAllBook();

    //get data by id
    @Result(property = "book_id", column = "book_id")

    @Select("SELECT * FROM books WHERE book_id =#{bookId}")
    @Result(property = "timestamp",column = "published_date")
    @Result(property = "author",column = "author_id",
            one = @One(select = "com.example.homeworkspring003.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories",column = "book_id",
            many = @Many(select = "com.example.homeworkspring003.repository.CategoryRepository.getCategoryByBookId")
    )
    Book getBookById(Integer bookId);

    //insert data
    @Result(property = "book_id", column = "book_id")

    @Select("INSERT INTO books(title,published_date,author_id)"+
            "VALUES (#{res.ttitle},#{res.importDate},#{res.authorId})"
    )
    Integer savebook(@Param("res") BookRequest bookRequest);

    @Result(property = "timestamp",column = "published_date")
    @Result(property = "author",column = "author_id",
            one = @One(select = "com.example.homeworkspring003.repository.AuthorRepository.getAuthorById")
    )
    @Result(property = "categories",column = "book_id",
            many = @Many(select = "com.example.homeworkspring003.repository.CategoryRepository.getCategoryByBookId")
    )
    Integer saveCategoryByBookId(Integer bookId,Integer categoryId);
}
