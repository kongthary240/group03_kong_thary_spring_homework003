package com.example.homeworkspring003.exception;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(Integer useId){
        super("User with id :"+useId+"not found");

    }
}
