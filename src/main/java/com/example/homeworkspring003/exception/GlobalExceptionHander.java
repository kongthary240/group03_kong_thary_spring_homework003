package com.example.homeworkspring003.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class GlobalExceptionHander extends ResponseEntityExceptionHandler {
    @ExceptionHandler(UserNotFoundException.class)
    ProblemDetail handlerUserNotFoundException(UserNotFoundException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
        problemDetail.setTitle("author_id not found exception");
        problemDetail.setDetail("Not found");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setTitle(URI.create("http://localhost:8080/api/v1/not_found").toString());
        return problemDetail;
    }
    @ExceptionHandler(InvalidFielException.class)
    ProblemDetail handlerUserNotFoundException(InvalidFielException e){
        ProblemDetail problemDetail=ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST,e.getMessage());
        problemDetail.setTitle("Bad request");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setTitle(URI.create("http://localhost:8080/api/v1/register/fild-empty").toString());
        return problemDetail;
    }
}
