package com.example.homeworkspring003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkSpring003Application {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkSpring003Application.class, args);
    }

}
