package com.example.homeworkspring003.service.ServiceImp;

import com.example.homeworkspring003.model.entity.Book;
import com.example.homeworkspring003.model.request.BookRequest;
import com.example.homeworkspring003.repository.BookRepository;
import com.example.homeworkspring003.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService {
    private final BookRepository bookRepository;

    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBook() {
        return bookRepository.findAllBook();
    }

    @Override
    public Book getBookById(Integer bookId) {
        return bookRepository.getBookById(bookId);
    }

    @Override
    public Integer addnewBook(BookRequest bookRequest) {
        return null;
    }
}
