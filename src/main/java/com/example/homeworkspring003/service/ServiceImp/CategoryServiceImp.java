package com.example.homeworkspring003.service.ServiceImp;

import com.example.homeworkspring003.model.entity.Category;
import com.example.homeworkspring003.model.request.CategoryRequest;
import com.example.homeworkspring003.repository.CategoryRepository;
import com.example.homeworkspring003.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategory()
    {
        return categoryRepository.findAllCategory();
    }

    @Override
    public Category getCategoryById(Integer categoryId) {

        return categoryRepository.getCategoryById(categoryId);
    }

    @Override
    public boolean deleteCategoryById(Integer categoryId) {

        return categoryRepository.deleteCategoryById(categoryId);
    }

    @Override
    public Integer addCategory(CategoryRequest categoryRequest) {
        Integer categoryId= categoryRepository.addCategory(categoryRequest);
        return categoryId;
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer categoryId) {
        Integer categoryIdUpdate =categoryRepository.updateCategory(categoryRequest,categoryId);
        return categoryIdUpdate;
    }

}
