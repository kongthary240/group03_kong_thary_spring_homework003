package com.example.homeworkspring003.service.ServiceImp;

import com.example.homeworkspring003.exception.UserNotFoundException;
import com.example.homeworkspring003.model.entity.Author;
import com.example.homeworkspring003.model.request.AuthorRequest;
import com.example.homeworkspring003.repository.AuthorRepository;
import com.example.homeworkspring003.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorServiceImp(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }


    @Override
    public List<Author> getAllAuthor() {
        return authorRepository.findAllAuthor();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        if(authorId!=null){
            return authorRepository.getAuthorById(authorId);

        }else throw new UserNotFoundException(authorId);
    }

    @Override
    public boolean deleteAuthorById(Integer authorId) {
        return authorRepository.deleteAuthorById(authorId);
    }

    @Override
    public Integer addAuthor(AuthorRequest authorRequest) {
        Integer authorId= authorRepository.addAuthor(authorRequest);
        return authorId;
    }

    @Override
    public Integer updateAuthor(AuthorRequest authorRequest, Integer authorId) {
        Integer categoryIdUpdate =authorRepository.updateAuthor(authorRequest,authorId);
        return categoryIdUpdate;
    }
}
