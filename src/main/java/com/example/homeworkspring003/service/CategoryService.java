package com.example.homeworkspring003.service;

import com.example.homeworkspring003.model.entity.Category;
import com.example.homeworkspring003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();
    Category getCategoryById(Integer categoryId);
    boolean deleteCategoryById(Integer categoryId);
    Integer addCategory(CategoryRequest categoryRequest);
    Integer updateCategory(CategoryRequest categoryRequest,Integer categoryId);
}
