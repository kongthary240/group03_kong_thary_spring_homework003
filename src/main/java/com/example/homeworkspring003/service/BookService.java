package com.example.homeworkspring003.service;

import com.example.homeworkspring003.model.entity.Book;
import com.example.homeworkspring003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();
    Book getBookById(Integer bookId);
    Integer addnewBook(BookRequest bookRequest);
}