package com.example.homeworkspring003.service;

import com.example.homeworkspring003.model.entity.Author;
import com.example.homeworkspring003.model.request.AuthorRequest;
import com.example.homeworkspring003.model.request.CategoryRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();
    Author getAuthorById(Integer authorId);
    boolean deleteAuthorById(Integer authorId);
    Integer addAuthor(AuthorRequest categoryRequest);
    Integer updateAuthor(AuthorRequest authorRequest,Integer authorId);



}
